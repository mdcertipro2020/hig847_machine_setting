USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscMachine]    Script Date: 4/8/2022 12:25:52 PM ******/
DROP TABLE [dbo].[viscMachine]
GO

/****** Object:  Table [dbo].[viscMachine]    Script Date: 4/8/2022 12:25:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscMachine](
	[MachineKey] [int] IDENTITY(1,1) NOT NULL,
	[LineKey] [int] NOT NULL,
	[MachineId] [varchar](32) NOT NULL,
	[MachineName] [varchar](256) NOT NULL,
	[MachineImageDirectionKey] [int] NULL,
	[MachineDimX] [int] NULL,
	[MachineDimY] [int] NULL,
 CONSTRAINT [PK_viscMachine] PRIMARY KEY CLUSTERED 
(
	[machineKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO