USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscSettingType]    Script Date: 4/8/2022 12:41:24 PM ******/
DROP TABLE [dbo].[viscSettingType]
GO

/****** Object:  Table [dbo].[viscSettingType]    Script Date: 4/8/2022 12:41:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscSettingType](
	[SettingTypeKey] [int] IDENTITY(1,1) NOT NULL,
	[SettingTypeId] [varchar](32) NOT NULL,
	[SettingTypeName] [varchar](256) NOT NULL,
	[SettingTypeValues] [varchar](512) NULL,
 CONSTRAINT [PK_viscSettingType] PRIMARY KEY CLUSTERED 
(
	[SettingTypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO