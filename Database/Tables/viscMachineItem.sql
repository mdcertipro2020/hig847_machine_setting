USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscMachineItem]    Script Date: 4/8/2022 11:55:16 AM ******/
DROP TABLE [dbo].[viscMachineItem]
GO

/****** Object:  Table [dbo].[viscMachineItem]    Script Date: 4/8/2022 11:55:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscMachineItem](
	[ItemKey] [int] NOT NULL,
	[ItemId] [varchar](30) NOT NULL,
	[MachineKey] [int] NOT NULL,
	[ItemShortDescription] [varchar](40) NULL,
	[ItemLongDescription] [varchar](256) NULL,
	[ItemThumbnailPath] [varchar](256) NULL,
 CONSTRAINT [PK_viscItem] PRIMARY KEY CLUSTERED 
(
	[ItemKey] ASC,
	[MachineKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO