USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscLine]    Script Date: 4/8/2022 12:22:28 PM ******/
DROP TABLE [dbo].[viscLine]
GO

/****** Object:  Table [dbo].[viscLine]    Script Date: 4/8/2022 12:22:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscLine](
	[LineKey] [int] IDENTITY(1,1) NOT NULL,
	[LocationKey] [int] NULL,
	[LineId] [varchar](32) NULL,
	[LineName] [varchar](256) NULL,
 CONSTRAINT [PK_viscLine] PRIMARY KEY CLUSTERED 
(
	[lineKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO