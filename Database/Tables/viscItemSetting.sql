USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscItemSetting]    Script Date: 4/8/2022 2:44:38 PM ******/
DROP TABLE [dbo].[viscItemSetting]
GO

/****** Object:  Table [dbo].[viscItemSetting]    Script Date: 4/8/2022 2:44:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscItemSetting](
	[ItemSettingKey] [int] IDENTITY(1,1) NOT NULL,
	[ItemKey] [int] NOT NULL,
	[MachineKey] [int] NOT NULL,
	[SettingKey] [int] NOT NULL,
	[SettingValue] [varchar](128) NULL,
	[SettingLowerLimit] [varchar](128) NULL,
	[SettingUpperLimit] [varchar](128) NULL,
 CONSTRAINT [PK_viscItemSetting] PRIMARY KEY CLUSTERED 
(
	[itemSettingKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO