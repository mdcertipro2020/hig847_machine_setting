USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscImage]    Script Date: 4/8/2022 11:36:05 AM ******/
DROP TABLE [dbo].[viscImage]
GO

/****** Object:  Table [dbo].[viscImage]    Script Date: 4/8/2022 11:36:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscImage](
	[ImageKey] [int] IDENTITY(1,1) NOT NULL,
	--[machineKey] [int] NULL,
	[OwnerKey] [int] NOT NULL,
	[OwnerType] [smallint] NOT NULL,
	[ImageId] [varchar](32) NULL,
	[ImageName] [varchar](128) NULL,
	[ImageFileName] [varchar](256) NULL,
 CONSTRAINT [PK_viscImage] PRIMARY KEY CLUSTERED 
(
	[imageKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


