USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscImageDirections]    Script Date: 4/8/2022 12:34:56 PM ******/
DROP TABLE [dbo].[viscImageDirections]
GO

/****** Object:  Table [dbo].[viscImageDirections]    Script Date: 4/8/2022 12:34:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscImageDirections](
	[ImageDirectionKey] [int] IDENTITY(1,1) NOT NULL,
	[ImageDirectionName] [varchar](32) NULL,
 CONSTRAINT [PK_viscImageDirections] PRIMARY KEY CLUSTERED 
(
	[ImageDirectionKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO