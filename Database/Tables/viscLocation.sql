USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscLocation]    Script Date: 4/8/2022 12:20:47 PM ******/
DROP TABLE [dbo].[viscLocation]
GO

/****** Object:  Table [dbo].[viscLocation]    Script Date: 4/8/2022 12:20:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscLocation](
	[LocationKey] [int] IDENTITY(1,1) NOT NULL,
	[LocationId] [varchar](32) NULL,
	[LocationName] [varchar](256) NULL,
 CONSTRAINT [PK_viscLocation] PRIMARY KEY CLUSTERED 
(
	[locationKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO