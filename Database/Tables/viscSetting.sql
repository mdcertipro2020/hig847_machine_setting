USE [HIG847_VisConfig]
GO

/****** Object:  Table [dbo].[viscSetting]    Script Date: 4/8/2022 12:42:58 PM ******/
DROP TABLE [dbo].[viscSetting]
GO

/****** Object:  Table [dbo].[viscSetting]    Script Date: 4/8/2022 12:42:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[viscSetting](
	[SettingKey] [int] IDENTITY(1,1) NOT NULL,
	[ImageKey] [int] NOT NULL,
	[SettingId] [varchar](32) NOT NULL,
	[SettingName] [varchar](256) NOT NULL,
	[SettingTypeKey] [int] NULL,
	[SettingMinVal] [varchar](128) NULL,
	[SettingMaxVal] [varchar](128) NULL,
	[SettingDefaultVal] [varchar](128) NULL,
	[SettingImageLocX] [int] NULL,
	[SettingImageLocY] [int] NULL,
	[SettingImageLocZ] [int] NULL,
	[settingVideoURL] [varchar](256) NULL,
 CONSTRAINT [PK_viscSetting] PRIMARY KEY CLUSTERED 
(
	[SettingKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO