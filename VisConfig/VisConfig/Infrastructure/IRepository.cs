﻿namespace VisConfig.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void Add(T item);
        void Update(T item);
        void Delete(T item);
        Task<int> SaveChangesAsync();
    }
}
