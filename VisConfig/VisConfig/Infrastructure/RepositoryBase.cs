﻿namespace VisConfig.Infrastructure
{
    using Data;
    public class RepositoryBase<T> : IRepository<T> where T : class
    {
        protected readonly HIG847_VisConfigContext _context;
        public RepositoryBase()
        {
            _context = new HIG847_VisConfigContext();
        }
        public virtual void Add(T item)
        {
             _context.Set<T>().Add(item);
        }
        public virtual void Delete(T item)
        {
            _context.Set<T>().Remove(item);
        }
        public virtual void Update(T item)
        {
            _context.Set<T>().Update(item);
        }
        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }        
    }
}
