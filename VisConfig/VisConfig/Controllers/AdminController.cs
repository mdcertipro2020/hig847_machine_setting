﻿using Microsoft.AspNetCore.Mvc;

namespace VisConfig.Controllers
{
    using Infrastructure;
    using Data;
    public class AdminController : ControllerBase
    {
        //IRepository<ViscLocation> repo;

        //public AdminController()
        //{
        //    repo = new RepositoryBase<ViscLocation>();
        //}

        public async Task<IActionResult> Index()
        {
            if (Repo != null)
            {
                //var repo = new RepositoryBase<ViscLocation>();
                Repo.Add(new ViscLocation()
                {
                    LocationId = "Location 1",
                    LocationName = "location name"
                });
                await Repo.SaveChangesAsync();
            }

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(ViscLocation model)
        {
            model = new ViscLocation()
            {
                LocationId = "Location 1",
                LocationName = "location name"
            };
            //var repo = new RepositoryBase<ViscLocation>();
            Repo.Add(model);
            await Repo.SaveChangesAsync();

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(ViscLine model)
        {
            model = new ViscLine()
            {
                LocationKey = 1,
                LineId = "line id",
                LineName = "Line name 1"
            };
            var repo = new RepositoryBase<ViscLine>();
            repo.Add(model);
            await repo.SaveChangesAsync();

            return View();
        }
    }
}
