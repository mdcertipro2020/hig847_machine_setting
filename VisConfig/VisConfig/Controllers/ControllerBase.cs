﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace VisConfig.Controllers
{
    using Infrastructure;
    public class ControllerBase : Controller
    {
        private dynamic repo = null;
        public override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var obj = new Data.ViscLocation();
            object t = null;
            try
            {
                if (context.ActionArguments.Count > 0)
                {
                    t = context.ActionArguments.First();
                }

                repo = Activator.CreateInstance(typeof(RepositoryBase<>).MakeGenericType(new Type[] { t.GetType() }));
            }
            catch
            {
                repo = null;
            }


            //context.Controller.GetType();
            return base.OnActionExecutionAsync(context, next);
        }

        protected dynamic Repo
        {
            get { return repo; }
        }
    }
}
