﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscLine
    {
        public int LineKey { get; set; }
        public int? LocationKey { get; set; }
        public string? LineId { get; set; }
        public string? LineName { get; set; }
    }
}
