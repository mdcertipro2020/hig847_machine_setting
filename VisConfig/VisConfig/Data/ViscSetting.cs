﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscSetting
    {
        public int SettingKey { get; set; }
        public int ImageKey { get; set; }
        public string SettingId { get; set; } = null!;
        public string SettingName { get; set; } = null!;
        public int? SettingTypeKey { get; set; }
        public string? SettingMinVal { get; set; }
        public string? SettingMaxVal { get; set; }
        public string? SettingDefaultVal { get; set; }
        public int? SettingImageLocX { get; set; }
        public int? SettingImageLocY { get; set; }
        public int? SettingImageLocZ { get; set; }
        public string? SettingVideoUrl { get; set; }
    }
}
