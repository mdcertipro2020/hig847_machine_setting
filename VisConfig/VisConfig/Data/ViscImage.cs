﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscImage
    {
        public int ImageKey { get; set; }
        public int OwnerKey { get; set; }
        public short OwnerType { get; set; }
        public string? ImageId { get; set; }
        public string? ImageName { get; set; }
        public string? ImageFileName { get; set; }
    }
}
