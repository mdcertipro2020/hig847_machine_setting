﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscMachineItem
    {
        public int ItemKey { get; set; }
        public string ItemId { get; set; } = null!;
        public int MachineKey { get; set; }
        public string? ItemShortDescription { get; set; }
        public string? ItemLongDescription { get; set; }
        public string? ItemThumbnailPath { get; set; }
    }
}
