﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscItemSetting
    {
        public int ItemSettingKey { get; set; }
        public int ItemKey { get; set; }
        public int MachineKey { get; set; }
        public int SettingKey { get; set; }
        public string? SettingValue { get; set; }
        public string? SettingLowerLimit { get; set; }
        public string? SettingUpperLimit { get; set; }
    }
}
