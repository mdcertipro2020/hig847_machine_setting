﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscImageDirection
    {
        public int ImageDirectionKey { get; set; }
        public string? ImageDirectionName { get; set; }
    }
}
