﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscMachine
    {
        public int MachineKey { get; set; }
        public int LineKey { get; set; }
        public string MachineId { get; set; } = null!;
        public string MachineName { get; set; } = null!;
        public int? MachineImageDirectionKey { get; set; }
        public int? MachineDimX { get; set; }
        public int? MachineDimY { get; set; }
    }
}
