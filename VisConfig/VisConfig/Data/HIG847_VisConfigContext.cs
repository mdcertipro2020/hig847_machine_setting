﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace VisConfig.Data
{
    public partial class HIG847_VisConfigContext : DbContext
    {
        public HIG847_VisConfigContext()
        {
        }

        public HIG847_VisConfigContext(DbContextOptions<HIG847_VisConfigContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ViscImage> ViscImages { get; set; } = null!;
        public virtual DbSet<ViscImageDirection> ViscImageDirections { get; set; } = null!;
        public virtual DbSet<ViscItemSetting> ViscItemSettings { get; set; } = null!;
        public virtual DbSet<ViscLine> ViscLines { get; set; } = null!;
        public virtual DbSet<ViscLocation> ViscLocations { get; set; } = null!;
        public virtual DbSet<ViscMachine> ViscMachines { get; set; } = null!;
        public virtual DbSet<ViscMachineItem> ViscMachineItems { get; set; } = null!;
        public virtual DbSet<ViscSetting> ViscSettings { get; set; } = null!;
        public virtual DbSet<ViscSettingType> ViscSettingTypes { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=CPS010;Initial Catalog=HIG847_VisConfig;User Id=admin; Password=;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ViscImage>(entity =>
            {
                entity.HasKey(e => e.ImageKey);

                entity.ToTable("viscImage");

                entity.Property(e => e.ImageFileName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ImageId)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.ImageName)
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViscImageDirection>(entity =>
            {
                entity.HasKey(e => e.ImageDirectionKey);

                entity.ToTable("viscImageDirections");

                entity.Property(e => e.ImageDirectionName)
                    .HasMaxLength(32)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViscItemSetting>(entity =>
            {
                entity.HasKey(e => e.ItemSettingKey);

                entity.ToTable("viscItemSetting");

                entity.Property(e => e.SettingLowerLimit)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SettingUpperLimit)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SettingValue)
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViscLine>(entity =>
            {
                entity.HasKey(e => e.LineKey);

                entity.ToTable("viscLine");

                entity.Property(e => e.LineId)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.LineName)
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViscLocation>(entity =>
            {
                entity.HasKey(e => e.LocationKey);

                entity.ToTable("viscLocation");

                entity.Property(e => e.LocationId)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.LocationName)
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViscMachine>(entity =>
            {
                entity.HasKey(e => e.MachineKey);

                entity.ToTable("viscMachine");

                entity.Property(e => e.MachineId)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.MachineName)
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViscMachineItem>(entity =>
            {
                entity.HasKey(e => new { e.ItemKey, e.MachineKey })
                    .HasName("PK_viscItem");

                entity.ToTable("viscMachineItem");

                entity.Property(e => e.ItemId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ItemLongDescription)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.ItemShortDescription)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ItemThumbnailPath)
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ViscSetting>(entity =>
            {
                entity.HasKey(e => e.SettingKey);

                entity.ToTable("viscSetting");

                entity.Property(e => e.SettingDefaultVal)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SettingId)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.SettingMaxVal)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SettingMinVal)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.SettingName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.SettingVideoUrl)
                    .HasMaxLength(256)
                    .IsUnicode(false)
                    .HasColumnName("settingVideoURL");
            });

            modelBuilder.Entity<ViscSettingType>(entity =>
            {
                entity.HasKey(e => e.SettingTypeKey);

                entity.ToTable("viscSettingType");

                entity.Property(e => e.SettingTypeKey).HasColumnName("settingTypeKey");

                entity.Property(e => e.SettingTypeId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("settingTypeId");

                entity.Property(e => e.SettingTypeName)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("settingTypeName");

                entity.Property(e => e.SettingTypeValues)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("settingTypeValues");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
