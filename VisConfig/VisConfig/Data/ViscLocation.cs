﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscLocation
    {
        public int LocationKey { get; set; }
        public string? LocationId { get; set; }
        public string? LocationName { get; set; }
    }
}
