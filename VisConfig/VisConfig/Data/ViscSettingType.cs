﻿using System;
using System.Collections.Generic;

namespace VisConfig.Data
{
    public partial class ViscSettingType
    {
        public int SettingTypeKey { get; set; }
        public string? SettingTypeId { get; set; }
        public string? SettingTypeName { get; set; }
        public string? SettingTypeValues { get; set; }
    }
}
